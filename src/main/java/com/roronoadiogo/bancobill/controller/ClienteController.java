package com.roronoadiogo.bancobill.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.impl.ClienteServiceImpl;
import com.roronoadiogo.bancobill.util.Response;

@RestController
@RequestMapping(path = "/api/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteServiceImpl clienteService;
	
	@RequestMapping(method = RequestMethod.POST )
	public ResponseEntity<Response<ClienteDto>> cadastrar(@Valid @RequestBody ClienteDto clienteDto, BindingResult result ){
		return clienteService.persistir(clienteDto, result);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Response<ClienteDto>> atualizar(@PathVariable("id") Long id, 
			@Valid @RequestBody ClienteDto clienteDto, BindingResult result){
		return clienteService.update(id, clienteDto, result);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<ClienteDto>> deletar(@PathVariable("id") Long id){
		return clienteService.deleta(id);
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<ClienteDto>> findByIdCliente(@PathVariable("id") Long id){
		return clienteService.findByIdCliente(id);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Response<ClienteDto>> listar(){
		return clienteService.listarTodos();
	}
	

}

package com.roronoadiogo.bancobill.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.dtos.OperacaoDto;
import com.roronoadiogo.bancobill.impl.ContaServiceImpl;
import com.roronoadiogo.bancobill.impl.OperacaoServiceImpl;
import com.roronoadiogo.bancobill.util.Response;

@RestController
@RequestMapping("/api/operacao")
public class OperacaoController {

	@Autowired
	private OperacaoServiceImpl operacaoService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Response<OperacaoDto>> cadastrar(@Valid @RequestBody OperacaoDto operacaoDto, BindingResult result) {
		return operacaoService.persistir(operacaoDto, result);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Response<OperacaoDto>> listar() {
		return operacaoService.listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<OperacaoDto>> buscar(@PathVariable("id") Long id) {
		return operacaoService.findByIdOperacao(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<OperacaoDto>> deletar(@PathVariable("id") Long id) {
		return operacaoService.deleta(id);
	}

}

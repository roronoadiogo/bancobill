package com.roronoadiogo.bancobill.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.roronoadiogo.bancobill.dtos.BancoDto;
import com.roronoadiogo.bancobill.impl.BancoServiceImpl;
import com.roronoadiogo.bancobill.util.Response;

@RestController
@RequestMapping(path = "/api/banco")
public class BancoController {

	@Autowired
	private BancoServiceImpl bancoService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Response<BancoDto>> cadastrar(@Valid @RequestBody BancoDto banco, BindingResult result) {
		return bancoService.persistir(banco, result);

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Response<BancoDto>> listar() {
		return bancoService.listarTodos();

	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<BancoDto>> atualizar(@PathVariable("id") Long id) {
		return bancoService.findByIdBanco(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Response<BancoDto>> atualizar(@PathVariable("id") Long id, @Valid @RequestBody BancoDto banco,
			BindingResult result) {
		return bancoService.update(id, banco, result);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<BancoDto>> deletar(@PathVariable("id") Long id){
		return bancoService.deleta(id);
	}

}

package com.roronoadiogo.bancobill.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.impl.AgenciaServiceImpl;
import com.roronoadiogo.bancobill.util.Response;

@RestController()
@RequestMapping(path = "/api/agencia")
public class AgenciaController {

	@Autowired
	private AgenciaServiceImpl agenciaService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Response<AgenciaDto>> cadastrar(@Valid @RequestBody AgenciaDto agenciaDto,
			BindingResult result) {
		return agenciaService.persistir(agenciaDto, result);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Response<AgenciaDto>> listar() {
		return agenciaService.listarTodos();
	}
	
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<AgenciaDto>> findByIdAgencia(@PathVariable("id") Long id) {
		return agenciaService.findByIdAgencia(id);
	}		
	
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Response<AgenciaDto>> atualizar (@PathVariable("id") Long id, @Valid @RequestBody AgenciaDto agenciaDto, BindingResult result) {
		return agenciaService.update(id, agenciaDto, result);
	}	
	
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<AgenciaDto>> deletar (@PathVariable("id") Long id) {
		return agenciaService.deleta(id);
	}	

}

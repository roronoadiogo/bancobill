package com.roronoadiogo.bancobill.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.roronoadiogo.bancobill.dtos.BacenDto;
import com.roronoadiogo.bancobill.impl.BacenServiceImpl;
import com.roronoadiogo.bancobill.util.Response;

@RestController
@RequestMapping(path = "/api/bacen")
public class BacenController {

	@Autowired
	private BacenServiceImpl bacenService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Response<BacenDto>> cadastrar(@Valid @RequestBody BacenDto bacen, BindingResult result) {
		return bacenService.persistir(bacen, result);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Response<BacenDto>> lista() {
		return bacenService.listarTodos();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<BacenDto>> buscarId(@PathVariable("id") Long idBacen) {
		return bacenService.findByIdBacen(idBacen);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Response<BacenDto>> atualiza(@PathVariable("id") Long idBacen, 
			@Valid @RequestBody BacenDto bacenDto, BindingResult result) {
		
		return bacenService.update(idBacen, bacenDto, result);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<BacenDto>> deletaId(@PathVariable("id") Long idBacen) {
		return bacenService.deleta(idBacen);
	}

}

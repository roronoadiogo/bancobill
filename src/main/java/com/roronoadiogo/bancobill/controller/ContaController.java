package com.roronoadiogo.bancobill.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.impl.ContaServiceImpl;
import com.roronoadiogo.bancobill.util.Response;

@RestController
@RequestMapping("/api/conta")
public class ContaController {

	@Autowired
	private ContaServiceImpl contaService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Response<ContaDto>> cadastrar(@Valid @RequestBody ContaDto contaDto, BindingResult result) {
		return contaService.persistir(contaDto, result);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Response<ContaDto>> listar() {
		return contaService.listarTodos();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Response<ContaDto>> atualizar(@PathVariable("id") Long id, @Valid @RequestBody ContaDto conta,
			BindingResult result) {
		return contaService.update(id, conta, result);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<ContaDto>> buscar(@PathVariable("id") Long id) {
		return contaService.findByIdConta(id);
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<ContaDto>> deletar(@PathVariable("id") Long id) {
		return contaService.deleta(id);
	}

}

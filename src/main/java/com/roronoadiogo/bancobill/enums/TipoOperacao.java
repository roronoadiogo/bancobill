package com.roronoadiogo.bancobill.enums;

public enum TipoOperacao {
	
	SACAR, TRANSFERIR, DEPOSITAR;

}

package com.roronoadiogo.bancobill.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.converter.ClienteConverterImpl;
import com.roronoadiogo.bancobill.dtos.BancoDto;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.entity.Banco;
import com.roronoadiogo.bancobill.entity.Cliente;
import com.roronoadiogo.bancobill.repository.ICliente;
import com.roronoadiogo.bancobill.services.IClienteService;
import com.roronoadiogo.bancobill.util.Response;

@Service
public class ClienteServiceImpl implements IClienteService{
	
	@Autowired
	private ICliente clienteDao;
	
	@Autowired
	private ClienteConverterImpl converter;

	private static final Logger log = LoggerFactory.getLogger(ClienteServiceImpl.class);
	
	@Override
	public ResponseEntity<Response<ClienteDto>> persistir(ClienteDto clienteDto, BindingResult result) {
		
		Response<ClienteDto> response = new Response<>();
		log.info("Persitindo Cliente: {}", clienteDto);
		
		if (result.hasErrors()) {
			log.info("Erro na validação padrão do cliente: {}", clienteDto);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		response.setData(clienteDto);
		clienteDao.save(converter.converterDtoToEntity(clienteDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@Override
	public ResponseEntity<Response<ClienteDto>> update(Long id, ClienteDto clienteDto, BindingResult result) {
		
		log.info("Atualizando cliente pelo id: {}", id);

		Response<ClienteDto> response = new Response<>();

		if (clienteDao.findById(id).isEmpty()) {
			log.info("Não encontrado o id do cliente para atualizar: {}", id);
			response.getErrors().add("Id do Cliente não encontrado para atualizar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		} else if (result.hasErrors()) {
			log.info("Cliente não pode ser atualizado: {}", id);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		Cliente cliente = new Cliente();
		cliente.setCpf(clienteDto.getCpf());
		cliente.setNome(clienteDto.getNome());
		cliente.setSobrenome(clienteDto.getSobrenome());
		cliente.setSenha(clienteDto.getSenha());
		
		clienteDao.save(cliente);
		response.setData(converter.converterEntityToDto(cliente));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<ClienteDto>> findByIdCliente(Long id) {
		
		log.info("Buscando por id Cliente: {}", id);
		
		Response<ClienteDto> response = new Response<ClienteDto>();
		
		if(clienteDao.findById(id).isEmpty()) {
			log.info("Buscando por id Cliente: {}", id);
			response.getErrors().add("idCliente nao encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);			
		}
		
		Cliente cliente = clienteDao.findById(id).get();
		response.setData(converter.converterEntityToDto(cliente));
		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}

	@Override
	public ResponseEntity<Response<ClienteDto>> deleta(Long id) {
		
		log.info("Deletando por id CLiente: {}", id);

		Response<ClienteDto> response = new Response<ClienteDto>();

		if (clienteDao.findById(id).isEmpty()) {
			log.info("Não encontrado id cliente para deletar: {}", id);
			response.getErrors().add("id cliente não encontrado para deletar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Cliente clienteEntity = clienteDao.findById(id).get();
		
		clienteDao.delete(clienteEntity);
		
		response.setData(converter.converterEntityToDto(clienteEntity));
		return ResponseEntity.status(HttpStatus.OK).body(response);
		
	}

	@Override
	public ResponseEntity<Response<ClienteDto>> listarTodos() {
		log.info("Listando Clientes");

		Response<ClienteDto> response = new Response<ClienteDto>();

		List<Cliente> clienteEntity = clienteDao.findAll();
		
		if (clienteEntity.isEmpty()) {
			response.getErrors().add("Não há Clientes cadastrados");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		response.setDatas(converter.converterEntityToDto(clienteEntity));
		return ResponseEntity.ok(response);
	}
	
	

}
 
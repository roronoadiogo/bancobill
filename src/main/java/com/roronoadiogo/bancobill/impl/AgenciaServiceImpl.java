package com.roronoadiogo.bancobill.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.converter.IAgenciaConverter;
import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.repository.IAgencia;
import com.roronoadiogo.bancobill.services.IAgenciaService;
import com.roronoadiogo.bancobill.util.Response;

@Service
public class AgenciaServiceImpl implements IAgenciaService {

	private static final Logger log = LoggerFactory.getLogger(AgenciaServiceImpl.class);

	@Autowired
	private IAgencia agenciaDao;

	@Autowired
	private IAgenciaConverter converter;

	@Override
	public ResponseEntity<Response<AgenciaDto>> persistir(AgenciaDto agenciaDto, BindingResult result) {
		log.info("Persitindo Agencia: {}", agenciaDto);

		Response<AgenciaDto> response = new Response<AgenciaDto>();

		if (result.hasErrors()) {

			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}

		response.setData(agenciaDto);
		agenciaDao.save(converter.converterDtoToEntity(agenciaDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	
	@Override
	public ResponseEntity<Response<AgenciaDto>> update(Long id, AgenciaDto agenciaDto, BindingResult result) {
		
		log.info("Atualizando por id Agencia: {}", id);

		Response<AgenciaDto> response = new Response<AgenciaDto>();

		if (agenciaDao.findById(id).isEmpty()) {
			log.info("Não encontrado id agencia para atualizar: {}", id);
			response.getErrors().add("Id agencia não encontrado para atualizar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		} else if (result.hasErrors()) {
			log.info("Id agencia não pode ser atualizado: {}", id);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		agenciaDao.save(converter.converterDtoToEntity(agenciaDto));

		response.setData(agenciaDto);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@Override
	public ResponseEntity<Response<AgenciaDto>> findByIdAgencia(Long id) {

		log.info("Buscando por idAgencia: {}", id);

		Response<AgenciaDto> response = new Response<AgenciaDto>();

		if (agenciaDao.findById(id).isEmpty()) {
			log.info("Não encontrado id da agencia: {}", id);
			response.getErrors().add("Id da agencia não encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		}

		Agencia agencia = agenciaDao.findById(id).get();
		response.setData(converter.converterEntityToDto(agencia));

		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@Override
	public ResponseEntity<Response<AgenciaDto>> deleta(Long id) {

		log.info("Deletando por id agencia: {}", id);

		Response<AgenciaDto> response = new Response<AgenciaDto>();

		if (agenciaDao.findById(id).isEmpty()) {
			log.info("Não encontrado id da agencia para deletar: {}", id);
			response.getErrors().add("id da agencia não encontrado para deletar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		}

		Agencia bacen = agenciaDao.findById(id).get();

		agenciaDao.delete(bacen);
		response.setData(converter.converterEntityToDto(bacen));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<AgenciaDto>> listarTodos() {
		
		log.info("Listando agências");

		Response<AgenciaDto> response = new Response<AgenciaDto>();
		
		List<Agencia> agenciaEntity = agenciaDao.findAll();

		if (agenciaEntity.isEmpty()) {
			response.getErrors().add("Não há Agencias cadastradas");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		response.setDatas(converter.converterEntityToDto(agenciaEntity));
		return ResponseEntity.ok(response);
		
	}

}

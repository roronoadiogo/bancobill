package com.roronoadiogo.bancobill.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.converter.BacenConverterImpl;
import com.roronoadiogo.bancobill.dtos.BacenDto;
import com.roronoadiogo.bancobill.entity.Bacen;
import com.roronoadiogo.bancobill.repository.IBacen;
import com.roronoadiogo.bancobill.services.IBacenService;
import com.roronoadiogo.bancobill.util.Response;

@Service
public class BacenServiceImpl implements IBacenService {

	private static final Logger log = LoggerFactory.getLogger(BacenServiceImpl.class);

	@Autowired
	private IBacen bacenDao;

	@Autowired
	private BacenConverterImpl converter;

	@Override
	public ResponseEntity<Response<BacenDto>> persistir(BacenDto bacenDto, BindingResult result) {
		log.info("Persitindo Bacen: {}", bacenDto);

		Response<BacenDto> response = new Response<BacenDto>();

		if (result.hasErrors()) {

			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);

		}

		response.setData(bacenDto);
		bacenDao.save(converter.converterDtoToEntity(bacenDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}

	@Override
	public ResponseEntity<Response<BacenDto>> update(Long id, BacenDto bacenDto, BindingResult result) {

		log.info("Atualizando por idBanco: {}", id);

		Response<BacenDto> response = new Response<BacenDto>();

		if (bacenDao.findById(id).isEmpty()) {
			log.info("Não encontrado idBacen para atualizar: {}", id);
			response.getErrors().add("IdBacen não encontrado para atualizar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		} else if (result.hasErrors()) {
			log.info("IdBacen não pode ser atualizado: {}", id);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		Bacen bacen = bacenDao.findById(id).get();
		bacen.setNome(bacenDto.getNome());
		bacenDao.save(bacen);

		response.setData(bacenDto);
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@Override
	public ResponseEntity<Response<BacenDto>> findByIdBacen(Long id) {

		log.info("Buscando por idBacen: {}", id);

		Response<BacenDto> response = new Response<BacenDto>();

		if (bacenDao.findById(id).isEmpty()) {
			log.info("Não encontrado idBacen: {}", id);
			response.getErrors().add("IdBacen não encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		}

		Bacen bacen = bacenDao.findById(id).get();
		
		response.setData(converter.converterEntityToDto(bacen));
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@Override
	public ResponseEntity<Response<BacenDto>> deleta(Long id) {

		log.info("Deletando por idBacen: {}", id);

		Response<BacenDto> response = new Response<BacenDto>();

		if (bacenDao.findById(id).isEmpty()) {
			log.info("Não encontrado idBacen para deletar: {}", id);
			response.getErrors().add("IdBacen não encontrado para deletar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		}

		Bacen bacen = bacenDao.findById(id).get();
		
		bacenDao.delete(bacen);
		response.setData(converter.converterEntityToDto(bacen));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	public ResponseEntity<Response<BacenDto>> listarTodos() {
		log.info("Listando Bacens");

		Response<BacenDto> response = new Response<BacenDto>();
		
		List<Bacen> bacenEntity = bacenDao.findAll();

		if (bacenEntity.isEmpty()) {
			response.getErrors().add("Não há Bacens cadastradas");

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		response.setDatas(converter.converterEntityToDto(bacenEntity));
		return ResponseEntity.ok(response);
	}

}

package com.roronoadiogo.bancobill.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.converter.ContaConverterImpl;
import com.roronoadiogo.bancobill.converter.OperacaoConverterImpl;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.dtos.OperacaoDto;
import com.roronoadiogo.bancobill.entity.Conta;
import com.roronoadiogo.bancobill.entity.Operacao;
import com.roronoadiogo.bancobill.repository.IConta;
import com.roronoadiogo.bancobill.repository.IOperacao;
import com.roronoadiogo.bancobill.services.IOperacaoService;
import com.roronoadiogo.bancobill.util.Response;

@Service
public class OperacaoServiceImpl implements IOperacaoService {

	@Autowired
	private IOperacao operacaoDao;

	@Autowired
	private OperacaoConverterImpl converter;

	private IConta contaDao;

	private static final Logger log = LoggerFactory.getLogger(OperacaoServiceImpl.class);

	@Override
	public ResponseEntity<Response<OperacaoDto>> persistir(OperacaoDto operacaoDto, BindingResult result) {
		
		Response<OperacaoDto> response = new Response<>();
		log.info("Persitindo Operacao: {}", operacaoDto);

		if (result.hasErrors()) {
			log.info("Erro na validação padrão da operacao: {}", operacaoDto);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		
		}
		
		response.setData(transferencia(operacaoDto).getData());
		
		operacaoDao.save(converter.converterDtoToEntity(operacaoDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
		
	}

	@Override
	public ResponseEntity<Response<OperacaoDto>> update(Long id, OperacaoDto operacaoDto, BindingResult result) {
		
		Response<OperacaoDto> response = new Response<>();
		log.info("Persitindo Operação: {}", operacaoDto);

		if (result.hasErrors()) {
			log.info("Erro na validação padrão da operacao: {}", operacaoDto);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		response.setData(operacaoDto);
		operacaoDao.save(converter.converterDtoToEntity(operacaoDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
		
	}

	@Override
	public ResponseEntity<Response<OperacaoDto>> findByIdOperacao(Long id) {
		log.info("Buscando por id da operacao: {}", id);

		Response<OperacaoDto> response = new Response<OperacaoDto>();

		if (operacaoDao.findById(id).isEmpty()) {
			log.info("Buscando por id Operacao: {}", id);
			response.getErrors().add("operacao não encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Operacao operacao = operacaoDao.findById(id).get();

		response.setData(converter.converterEntityToDto(operacao));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<OperacaoDto>> deleta(Long id) {
		
		log.info("Deletando por id Operacao: {}", id);

		Response<OperacaoDto> response = new Response<OperacaoDto>();

		if (operacaoDao.findById(id).isEmpty()) {
			log.info("Não encontrado id conta para deletar: {}", id);
			response.getErrors().add("id cliente não encontrado para deletar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Operacao operacaoEntity = operacaoDao.findById(id).get();

		operacaoDao.delete(operacaoEntity);

		response.setData(converter.converterEntityToDto(operacaoEntity));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<OperacaoDto>> listarTodos() {
		
		log.info("Listando Operacoes");

		Response<OperacaoDto> response = new Response<OperacaoDto>();

		List<Operacao> operacoes = operacaoDao.findAll();

		if (operacaoDao.findAll().isEmpty()) {
			response.getErrors().add("Não há operacoes");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		response.setDatas(converter.converterEntityToDto(operacoes));
		return ResponseEntity.ok(response);
	}
	
	
	public Response<OperacaoDto> transferencia(OperacaoDto operacao) {
				
		Response<OperacaoDto> response = new Response<>();
		
		if(operacao.getTipo().equals(1));
		log.info("Realizando transferencia codConta: {}", operacao.getContaOrigem().getCodConta());
		
		Conta origem = contaDao.findById(operacao.getContaOrigem().getIdConta()).get();
		Conta destinoConta = contaDao.findById(operacao.getContaDestino().getIdConta()).get();;
		
		if(operacao.getContaOrigem().getSaldo().compareTo(operacao.getValorOperacao()) == 1) {
			operacao.getContaOrigem().getSaldo().subtract(operacao.getValorOperacao());
			
			origem.getSaldo().subtract(operacao.getValorOperacao());
			destinoConta.getSaldo().add(operacao.getValorOperacao());
			
			contaDao.save(origem);
		
			response.setData(operacao);
			return response;
		}
				
		response.getErrors().add("Saldo da conta origem é insuficiente");
		return response;
		
	}
	

}

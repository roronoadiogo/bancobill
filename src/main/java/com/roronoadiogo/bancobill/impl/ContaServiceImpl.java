package com.roronoadiogo.bancobill.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.converter.ContaConverterImpl;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.entity.Conta;
import com.roronoadiogo.bancobill.repository.IConta;
import com.roronoadiogo.bancobill.services.IContaService;
import com.roronoadiogo.bancobill.util.Response;

@Service
public class ContaServiceImpl implements IContaService {

	@Autowired
	private IConta contaDao;

	@Autowired
	private ContaConverterImpl converter;

	private static final Logger log = LoggerFactory.getLogger(ContaServiceImpl.class);

	@Override
	public ResponseEntity<Response<ContaDto>> persistir(ContaDto contaDto, BindingResult result) {

		Response<ContaDto> response = new Response<>();
		log.info("Persitindo Conta: {}", contaDto);

		if (result.hasErrors()) {
			log.info("Erro na validação padrão do banco: {}", contaDto);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		response.setData(contaDto);
		contaDao.save(converter.converterDtoToEntity(contaDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@Override
	public ResponseEntity<Response<ContaDto>> update(Long id, ContaDto contaDto, BindingResult result) {

		log.info("Atualizando conta pelo id: {}", id);

		Response<ContaDto> response = new Response<>();

		if (contaDao.findById(id).isEmpty()) {
			log.info("Não encontrado o id da conta para atualizar: {}", id);
			response.getErrors().add("Id da conta não encontrado para atualizar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		} else if (result.hasErrors()) {
			log.info("Conta não pode ser atualizado: {}", id);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		Conta conta = contaDao.findById(id).get();
		conta.setLimiteDiario(contaDto.getLimiteDiario());
		conta.setSaldo(contaDto.getSaldo());
		conta.setLimiteDiario(contaDto.getLimiteDiario());

		contaDao.save(conta);
		response.setData(converter.converterEntityToDto(conta));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<ContaDto>> findByIdConta(Long id) {

		log.info("Buscando por id da Conta: {}", id);

		Response<ContaDto> response = new Response<ContaDto>();

		if (contaDao.findById(id).isEmpty()) {
			log.info("Buscando por id conta: {}", id);
			response.getErrors().add("idConta não encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Conta conta = contaDao.findById(id).get();

		response.setData(converter.converterEntityToDto(conta));
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@Override
	public ResponseEntity<Response<ContaDto>> deleta(Long id) {
		log.info("Deletando por id Conta: {}", id);

		Response<ContaDto> response = new Response<ContaDto>();

		if (contaDao.findById(id).isEmpty()) {
			log.info("Não encontrado id conta para deletar: {}", id);
			response.getErrors().add("id cliente não encontrado para deletar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Conta contaEntity = contaDao.findById(id).get();

		contaDao.delete(contaEntity);

		response.setData(converter.converterEntityToDto(contaEntity));
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<ContaDto>> listarTodos() {

		log.info("Listando Contas");

		Response<ContaDto> response = new Response<ContaDto>();

		List<Conta> contaEntity = contaDao.findAll();

		if (contaDao.findAll().isEmpty()) {
			response.getErrors().add("Não há Contas cadastrados");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		response.setDatas(converter.converterEntityToDto(contaEntity));
		return ResponseEntity.ok(response);
	}

}

package com.roronoadiogo.bancobill.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.converter.BancoConverterImpl;
import com.roronoadiogo.bancobill.dtos.BancoDto;
import com.roronoadiogo.bancobill.entity.Banco;
import com.roronoadiogo.bancobill.repository.IBanco;
import com.roronoadiogo.bancobill.services.IBancoService;
import com.roronoadiogo.bancobill.util.Response;

@Service
public class BancoServiceImpl implements IBancoService {

	@Autowired
	private IBanco bancoDao;

	@Autowired
	private BancoConverterImpl converter;

	private static final Logger log = LoggerFactory.getLogger(BancoServiceImpl.class);

	@Override
	public ResponseEntity<Response<BancoDto>> persistir(BancoDto bancoDto, BindingResult result) {

		Response<BancoDto> response = new Response<>();
		log.info("Persitindo Banco: {}", bancoDto);

		if (result.hasErrors()) {
			log.info("Erro na validação padrão do Banco: {}", bancoDto);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		response.setData(bancoDto);
		bancoDao.save(converter.converterDtoToEntity(bancoDto));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@Override
	public ResponseEntity<Response<BancoDto>> listarTodos() {
		log.info("Listando Bancos");

		Response<BancoDto> response = new Response<BancoDto>();

		List<Banco> bancosEntity = bancoDao.findAll();
		
		if (bancosEntity.isEmpty()) {
			response.getErrors().add("Não há Bancos cadastrados");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		response.setDatas(converter.converterEntityToDto(bancosEntity));
		return ResponseEntity.ok(response);
	}

	@Override
	public ResponseEntity<Response<BancoDto>> findByIdBanco(Long id) {

		log.info("Buscando por idBanco: {}", id);

		Response<BancoDto> response = new Response<BancoDto>();

		if (bancoDao.findById(id).isEmpty()) {

			log.info("Não encontrado idbanco: {}", id);
			response.getErrors().add("IdBanco não encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		}

		Banco bancoEntity = bancoDao.findById(id).get();
	
		response.setData(converter.converterEntityToDto(bancoEntity));

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Response<BancoDto>> deleta(Long id) {
		log.info("Deletando por idBanco: {}", id);

		Response<BancoDto> response = new Response<BancoDto>();

		if (bancoDao.findById(id).isEmpty()) {
			log.info("Não encontrado idbanco para deletar: {}", id);
			response.getErrors().add("IdBanco não encontrado para deletar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Banco bancoEntity = bancoDao.findById(id).get();
		
		bancoDao.delete(bancoEntity);
		
		response.setData(converter.converterEntityToDto(bancoEntity));
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@Override
	public ResponseEntity<Response<BancoDto>> update(Long id, BancoDto bancoDto, BindingResult result) {

		log.info("Atualizando banco pelo id: {}", id);

		Response<BancoDto> response = new Response<>();

		if (bancoDao.findById(id).isEmpty()) {
			log.info("Não encontrado o id do banco para atualizar: {}", id);
			response.getErrors().add("Id do Banco não encontrado para atualizar");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

		} else if (result.hasErrors()) {
			log.info("Banco não pode ser atualizado: {}", id);
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		Banco banco = bancoDao.findById(id).get();

		banco.setNomeFantasia(bancoDto.getNomeFantasia());
		banco.setCnpj(bancoDto.getCnpj());
		banco.setCodBanco(bancoDto.getCodBanco());
		banco.setRazaoSocial(bancoDto.getRazaoSocial());

		bancoDao.save(banco);
		
		response.setData(converter.converterEntityToDto(banco));
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

}

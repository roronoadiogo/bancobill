package com.roronoadiogo.bancobill.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.roronoadiogo.bancobill.entity.Cliente;

public interface ICliente extends JpaRepository<Cliente, Long>{

}

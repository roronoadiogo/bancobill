package com.roronoadiogo.bancobill.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;

import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.entity.Conta;
import com.roronoadiogo.bancobill.util.Response;

public interface IConta extends JpaRepository<Conta, Long>{

	
	
}

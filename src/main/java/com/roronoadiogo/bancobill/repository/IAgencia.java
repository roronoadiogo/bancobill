package com.roronoadiogo.bancobill.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.roronoadiogo.bancobill.entity.Agencia;

public interface IAgencia extends JpaRepository<Agencia, Long> {

}

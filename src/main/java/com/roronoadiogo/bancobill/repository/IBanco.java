package com.roronoadiogo.bancobill.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.roronoadiogo.bancobill.entity.Banco;

public interface IBanco extends JpaRepository<Banco, Long> {

}

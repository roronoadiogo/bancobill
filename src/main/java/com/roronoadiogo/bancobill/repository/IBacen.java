package com.roronoadiogo.bancobill.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.roronoadiogo.bancobill.entity.Bacen;

public interface IBacen extends JpaRepository<Bacen, Long>{

}

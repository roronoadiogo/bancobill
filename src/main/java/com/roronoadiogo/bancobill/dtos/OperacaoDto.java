package com.roronoadiogo.bancobill.dtos;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.roronoadiogo.bancobill.entity.Conta;
import com.roronoadiogo.bancobill.enums.TipoOperacao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperacaoDto {
	
	private Long idOperacao;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private Date diaOperacao;
	
	@JsonFormat(pattern="HH:mm:ss")
	private Date horaOperacao;
	
	private TipoOperacao tipo;
	
	private BigDecimal valorOperacao;

	private ContaDto contaOrigem;

	private ContaDto contaDestino;

}

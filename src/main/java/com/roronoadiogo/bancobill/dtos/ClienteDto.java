package com.roronoadiogo.bancobill.dtos;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDto {
	
	private Long idCliente;
	
	@NotEmpty(message = "Nome não pode ficar vazio")
	private String nome;
	@NotEmpty(message = "sobrenome não pode ficar vazio")
	private String sobrenome;
	@NotEmpty(message = "Cpf não pode ficar vazio")
	@CPF(message = "Cpf inválido")
	private String cpf;
	
	@Email(message = "email não válido")
	private String email;
	
	@NotEmpty(message = "Não pode ficar vazio")
	private String senha;
	
	private List<ContaDto> contas;

}

package com.roronoadiogo.bancobill.dtos;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CNPJ;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.entity.Bacen;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BancoDto {

	private Long idBanco;

	@NotEmpty(message = "Razão social não pode ficar vazia")
	private String razaoSocial;

	@NotEmpty(message = "Nome fantasia não pode ficar vazio")
	@Length(message = "Digite entre 3 a 45 caracteres")
	private String nomeFantasia;

	@NotEmpty(message = "Cnpj Não pode ficar vazio")
	@CNPJ(message = "Cnpj inválido")
	private String cnpj;

	@NotNull(message = "Não pode ficar nulo")
	private Long codBanco;
	
	private List<Agencia> agencias;

}

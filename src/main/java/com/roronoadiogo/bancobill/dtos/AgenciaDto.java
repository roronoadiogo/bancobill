package com.roronoadiogo.bancobill.dtos;

import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AgenciaDto {
	
	private Long idAgencia;
	
	@NotNull(message = "Não pode ficar vazio o cod da agencia")
	@Digits(message = "Não pode ficar vazio o cod da agencia", fraction = 0, integer = 4)
	private Long codAgencia;
	
	@NotEmpty(message = "Agencia deve ter um nome")
	@Size(min = 3, max = 45, message = "A agencia deve ter um nome")
	private String nomeAgencia;
	
	//private List<ClienteDto> clientes;
	
}

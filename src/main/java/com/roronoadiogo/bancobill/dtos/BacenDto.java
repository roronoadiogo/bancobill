package com.roronoadiogo.bancobill.dtos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BacenDto {

	private Long idBacen;
	private String nome;
	private List<BancoDto> bancos;

}

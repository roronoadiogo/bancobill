package com.roronoadiogo.bancobill.dtos;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContaDto {
	
	
	private Long idConta;
	@Digits(message = "codConta inválido", fraction = 0, integer = 5)
	private int codConta;
	@NotEmpty(message = "não pode ficar vazio a senha")
	private String senha;
	
	private BigDecimal saldo;
		
	private BigDecimal limiteDiario;
	
	private Date aberturaConta;
	
	private Date ultimaMovimentacao;

}

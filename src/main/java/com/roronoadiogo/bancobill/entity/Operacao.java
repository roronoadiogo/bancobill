package com.roronoadiogo.bancobill.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.roronoadiogo.bancobill.enums.TipoOperacao;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "operacao")
@Getter
@Setter
public class Operacao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_operacao")
	private Long idOperacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dia_operacao")
	private Date diaOperacao;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "hora_operacao")
	private Date horaOperacao;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "tipo_operacao")
	private TipoOperacao tipo;
	
	@Column(name = "valor_operacao")
	private BigDecimal valorOperacao;
	
	@ManyToOne()
	private Conta contaOrigem;

	@ManyToOne()
	private Conta contaDestino;
	
	@PrePersist
	public void setDiaOperacao() {
		this.horaOperacao = new Date();
		this.diaOperacao = new Date();
	}
}

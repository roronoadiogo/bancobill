package com.roronoadiogo.bancobill.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cliente")
@NoArgsConstructor
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente")
	private Long idCliente;

	@Column(name = "nome_cliente")
	private String nome;
	
	@Column(name = "sobrenome_cliente")
	private String sobrenome;
	
	@Column(name = "cpf_cliente")
	private String cpf;

	@Column(name = "email_cliente")
	private String email;

	@Column(name = "senha_cliente")
	private String senha;
	
	@ManyToOne()
	@JsonBackReference
	private Agencia agencia;
	
	@OneToMany(mappedBy = "cliente")
	@JsonManagedReference
	private List<Conta> contas;
	
}

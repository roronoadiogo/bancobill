package com.roronoadiogo.bancobill.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "agencia")
@Getter
@Setter
public class Agencia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_agencia")	
	private Long idAgencia;
	@Column(name = "cod_agencia", nullable = false)
	private Long codAgencia;
	@Column(name = "nome_agencia", nullable = false)
	private String nomeAgencia;
	
	@ManyToOne()
	@JsonBackReference
	private Banco banco;
	
	@OneToMany(mappedBy = "agencia")
	@JsonManagedReference
	private List<Cliente> clientes;
		
}

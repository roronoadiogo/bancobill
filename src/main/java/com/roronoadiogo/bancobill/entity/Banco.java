package com.roronoadiogo.bancobill.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "banco")
@Getter
@Setter
@NoArgsConstructor
public class Banco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_banco")
	private Long idBanco;
	@Column(name = "razao_social_banco", nullable = false)
	private String razaoSocial;
	@Column(name = "nome_fantasia", nullable = false)
	private String nomeFantasia;
	@Column(name = "cnpj_banco", nullable = false)
	private String cnpj;
	@Column(name = "cod_banco", nullable = false)
	private Long codBanco;
	
	@ManyToOne()
	@JoinColumn(name = "id_bacen")
	private Bacen bacen;
	
	@OneToMany(mappedBy = "banco")
	private List<Agencia> agencias;
	
	
}

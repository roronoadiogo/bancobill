package com.roronoadiogo.bancobill.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "conta")
public class Conta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_conta")
	private Long idConta;
	@Column(name = "cod_conta")
	private int codConta;
	@Column(name = "senha_conta")
	private String senha;
	@Column(name = "saldo_conta")
	private BigDecimal saldo;
	@Column(name = "limite_conta")
	private BigDecimal limiteDiario;
	@Column(name = "abertura_conta")
	private Date aberturaConta;
	@Column(name = "ultima_movimenta_conta")
	private Date ultimaMovimentacao;
		
	@ManyToOne
	@JsonBackReference
	private Cliente cliente;
	
	@PreUpdate
	public void ultimaAtt() {
		ultimaMovimentacao = new Date();
	}
	
	@PrePersist
	public void abertConta() {
		Date dataAtual = new Date();
		aberturaConta = dataAtual;
		ultimaMovimentacao = dataAtual;
	}
		
}

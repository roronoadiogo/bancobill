package com.roronoadiogo.bancobill.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.util.Response;

public interface IClienteService {
	
	ResponseEntity<Response<ClienteDto>> persistir(ClienteDto clienteDto, BindingResult result);
	
	ResponseEntity<Response<ClienteDto>> update(Long id, ClienteDto clienteDto, BindingResult result);
	
	ResponseEntity<Response<ClienteDto>> findByIdCliente(Long id);
	
	ResponseEntity<Response<ClienteDto>> deleta(Long id);
	
	ResponseEntity<Response<ClienteDto>> listarTodos();
	

}

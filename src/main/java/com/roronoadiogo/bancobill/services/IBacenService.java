package com.roronoadiogo.bancobill.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.dtos.BacenDto;
import com.roronoadiogo.bancobill.util.Response;

public interface IBacenService {

	ResponseEntity<Response<BacenDto>> persistir(BacenDto bacen, BindingResult result);
		
	ResponseEntity<Response<BacenDto>> listarTodos();	
	
	ResponseEntity<Response<BacenDto>> findByIdBacen(Long id);
	
	ResponseEntity<Response<BacenDto>> deleta(Long id);

	ResponseEntity<Response<BacenDto>> update(Long id, BacenDto bacenDto, BindingResult result);
	
	
}

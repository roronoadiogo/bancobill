package com.roronoadiogo.bancobill.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.dtos.BancoDto;
import com.roronoadiogo.bancobill.util.Response;

public interface IBancoService {

	ResponseEntity<Response<BancoDto>> persistir(BancoDto banco, BindingResult result);
	
	ResponseEntity<Response<BancoDto>> update(Long id, BancoDto bancoDto, BindingResult result);
	
	ResponseEntity<Response<BancoDto>> findByIdBanco(Long id);
	
	ResponseEntity<Response<BancoDto>> deleta(Long id);

	ResponseEntity<Response<BancoDto>> listarTodos();	
	
}

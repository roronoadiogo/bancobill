package com.roronoadiogo.bancobill.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.dtos.OperacaoDto;
import com.roronoadiogo.bancobill.entity.Operacao;
import com.roronoadiogo.bancobill.util.Response;

public interface IOperacaoService {
	
	ResponseEntity<Response<OperacaoDto>> persistir(OperacaoDto operacaoDto, BindingResult result);
	
	ResponseEntity<Response<OperacaoDto>> update(Long id, OperacaoDto operacaoDto, BindingResult result);
	
	ResponseEntity<Response<OperacaoDto>> findByIdOperacao(Long id);
	
	ResponseEntity<Response<OperacaoDto>> deleta(Long id);
	
	ResponseEntity<Response<OperacaoDto>> listarTodos();
	

}

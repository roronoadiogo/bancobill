package com.roronoadiogo.bancobill.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.util.Response;

public interface IAgenciaService {
	
	ResponseEntity<Response<AgenciaDto>> persistir(AgenciaDto agencia, BindingResult result);
	
	ResponseEntity<Response<AgenciaDto>> update(Long id, AgenciaDto agenciaDto, BindingResult result);
	
	ResponseEntity<Response<AgenciaDto>> findByIdAgencia(Long id);
	
	ResponseEntity<Response<AgenciaDto>> deleta(Long id);
	
	ResponseEntity<Response<AgenciaDto>> listarTodos();
	

}

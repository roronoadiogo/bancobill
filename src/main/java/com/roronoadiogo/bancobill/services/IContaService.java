package com.roronoadiogo.bancobill.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.util.Response;

public interface IContaService {
	
	ResponseEntity<Response<ContaDto>> persistir(ContaDto contaDto, BindingResult result);
	
	ResponseEntity<Response<ContaDto>> update(Long id, ContaDto contaDto, BindingResult result);
	
	ResponseEntity<Response<ContaDto>> findByIdConta(Long id);
	
	ResponseEntity<Response<ContaDto>> deleta(Long id);
	
	ResponseEntity<Response<ContaDto>> listarTodos();
	

}

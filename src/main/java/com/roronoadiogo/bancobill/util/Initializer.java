package com.roronoadiogo.bancobill.util;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.entity.Bacen;
import com.roronoadiogo.bancobill.entity.Banco;
import com.roronoadiogo.bancobill.entity.Cliente;
import com.roronoadiogo.bancobill.entity.Conta;
import com.roronoadiogo.bancobill.entity.Operacao;
import com.roronoadiogo.bancobill.enums.TipoOperacao;
import com.roronoadiogo.bancobill.repository.IAgencia;
import com.roronoadiogo.bancobill.repository.IBacen;
import com.roronoadiogo.bancobill.repository.IBanco;
import com.roronoadiogo.bancobill.repository.ICliente;
import com.roronoadiogo.bancobill.repository.IConta;
import com.roronoadiogo.bancobill.repository.IOperacao;

@Component
public class Initializer implements ApplicationListener<ContextRefreshedEvent> {

	private static final String Cliente = null;

	@Autowired
	private IBacen bacenDao;
	
	@Autowired
	private IBanco bancoDao;
	
	@Autowired
	private IAgencia agenciaDao;
	
	@Autowired
	private ICliente clienteDao;
	
	@Autowired
	private IConta contaDao;
	
	@Autowired
	private IOperacao operacaoDao;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {


			createBacen();
			createBanco();
			createAgencia();
			createCliente();
			createConta();
		
	}

	public void createBacen() {

		Bacen bac1 = new Bacen();
		bac1.setNome("Instituiçao bancária Brasileira");

		Bacen bac2 = new Bacen();
		bac2.setNome("Instituiçao bancária Americana");

		Bacen bac3 = new Bacen();
		bac3.setNome("Instituiçao bancária Reino Unido");

		bacenDao.save(bac1);
		bacenDao.save(bac2);
		bacenDao.save(bac3);

	}

	public void createBanco() {

		Banco bancoDoBrasil = new Banco();
		bancoDoBrasil.setBacen(bacenDao.findById(1L).get());
		bancoDoBrasil.setCnpj("63.432.751/0001-05");
		bancoDoBrasil.setCodBanco(001L);
		bancoDoBrasil.setNomeFantasia("Banco do Brasil");
		bancoDoBrasil.setRazaoSocial("BBC Credito Brasil");
		
		Banco bancoBradesco = new Banco();
		bancoBradesco.setBacen(bacenDao.findById(1L).get());
		bancoBradesco.setCnpj("54.652.887/0001-05");
		bancoBradesco.setCodBanco(237L);
		bancoBradesco.setRazaoSocial("Br Investimentos credito");
		bancoBradesco.setNomeFantasia("Banco Bradesco");
		
		bancoDao.save(bancoBradesco);
		bancoDao.save(bancoDoBrasil);
		
	}
	
	public void createAgencia() {

		Agencia agencia = new Agencia();
		agencia.setNomeAgencia("Banco do Brasil - Centro");
		agencia.setCodAgencia(124L);
		agencia.setBanco(bancoDao.findById(1L).get());
		
		Agencia agenciaBr = new Agencia();
		agenciaBr.setNomeAgencia("Bradesco - Sul");
		agenciaBr.setCodAgencia(235L);
		agenciaBr.setBanco(bancoDao.findById(1L).get());
		
		agenciaDao.save(agencia);
		agenciaDao.save(agenciaBr);
				
	}
	
	public void createCliente() {
		
		Cliente cliente = new Cliente();
		cliente.setNome("ViniciuS Diogo");
		cliente.setAgencia(agenciaDao.findById(1L).get());
		cliente.setEmail("roronoadiogo@gmail.com");
		cliente.setSobrenome("Alves");
		cliente.setCpf("03221221388");
		cliente.setSenha("asasas");
		
		Cliente cliente1 = new Cliente();
		cliente1.setNome("Adri Miguelista");
		cliente1.setAgencia(agenciaDao.findById(1L).get());
		cliente1.setEmail("adri@gmail.com");
		cliente1.setSobrenome("Alves");
		cliente1.setCpf("03221221388");
		cliente.setSenha("asasd");
				
		clienteDao.save(cliente);
		clienteDao.save(cliente1);
		
		
	}
	
	public void createConta() {
		
		Conta conta = new Conta();
		
		conta.setCodConta(471593);
		conta.setSaldo(new BigDecimal(200));
		conta.setLimiteDiario(new BigDecimal(800));
		conta.setSenha("Diogo");
		conta.setCliente(clienteDao.findById(1L).get());
				
		contaDao.save(conta);
				
		Conta conta2 = new Conta();
		conta2.setCodConta(43541);
		conta2.setSaldo(new BigDecimal(100));
		conta2.setLimiteDiario(new BigDecimal(800));
		conta2.setSenha("Diogo");
		
		conta2.setCliente(clienteDao.findById(2L).get());
		
		contaDao.save(conta2);
		
		Operacao operacao = new Operacao();
		
		operacao.setContaOrigem(contaDao.findById(1L).get());
		operacao.setContaDestino(contaDao.findById(2L).get());
		operacao.setTipo(TipoOperacao.TRANSFERIR);
		operacao.setValorOperacao(new BigDecimal(300.00));
		
		operacaoDao.save(operacao);
	
	}
	


}

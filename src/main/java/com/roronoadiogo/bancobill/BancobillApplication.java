package com.roronoadiogo.bancobill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancobillApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancobillApplication.class, args);
	}

}

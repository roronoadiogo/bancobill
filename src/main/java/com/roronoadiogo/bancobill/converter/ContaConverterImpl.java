package com.roronoadiogo.bancobill.converter;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.reflect.TypeToken;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.entity.Cliente;
import com.roronoadiogo.bancobill.entity.Conta;

@Component
public class ContaConverterImpl implements IContaConverter{
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public Conta converterDtoToEntity(ContaDto contaDto) {
		return mapper.map(contaDto, Conta.class);
	}

	@Override
	public ContaDto converterEntityToDto(Conta conta) {
		return mapper.map(conta, ContaDto.class);
	}

	@Override
	public List<Conta> converterDtoToEntity(List<ContaDto> contaDtos) {
		Type list = new TypeToken<List<Conta>>() {}.getType();
		List<Conta> lista = mapper.map(contaDtos, list); 
		return lista;
	}

	@Override
	public List<ContaDto> converterEntityToDto(List<Conta> conta) {
		Type list = new TypeToken<List<Conta>>() {}.getType();
		List<ContaDto> listDto = mapper.map(conta, list);
		return listDto;
	}
	
	

}

package com.roronoadiogo.bancobill.converter;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.reflect.TypeToken;
import com.roronoadiogo.bancobill.dtos.BancoDto;
import com.roronoadiogo.bancobill.entity.Banco;

@Component
public class BancoConverterImpl implements IBancoConverter{
	
	@Autowired
	private ModelMapper mapper;
	
	@Override
	public Banco converterDtoToEntity(BancoDto bancoDto) {
		return mapper.map(bancoDto, Banco.class);
	}

	@Override
	public BancoDto converterEntityToDto(Banco banco) {
		return mapper.map(banco, BancoDto.class);
	}

	@Override
	public List<Banco> converterDtoToEntity(List<BancoDto> bancos) {
		
		Type list = new TypeToken<List<BancoDto>>() {}.getType();
		List<Banco>	bancosEntity = mapper.map(bancos, list);	
		return bancosEntity;
	}

	@Override
	public List<BancoDto> converterEntityToDto(List<Banco> bancos) {
		Type list = new TypeToken<List<Banco>>() {}.getType();
		List<BancoDto> bancoDto = mapper.map(bancos, list);
		return bancoDto;
	}

}

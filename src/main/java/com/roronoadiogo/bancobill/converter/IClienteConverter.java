package com.roronoadiogo.bancobill.converter;

import java.util.List;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.entity.Cliente;

public interface IClienteConverter {
	
	Cliente converterDtoToEntity(ClienteDto clienteDto);

	ClienteDto converterEntityToDto(Cliente cliente);
	
	List<Cliente> converterDtoToEntity(List<ClienteDto> clienteDtos);
	
	List<ClienteDto> converterEntityToDto(List<Cliente> clientes);

}

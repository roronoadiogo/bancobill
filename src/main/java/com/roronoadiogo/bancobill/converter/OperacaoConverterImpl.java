package com.roronoadiogo.bancobill.converter;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.reflect.TypeToken;
import com.roronoadiogo.bancobill.dtos.OperacaoDto;
import com.roronoadiogo.bancobill.entity.Operacao;

@Component
public class OperacaoConverterImpl implements IOperacaoConverter{
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public Operacao converterDtoToEntity(OperacaoDto operacaoDto) {
		return mapper.map(operacaoDto, Operacao.class);
	}

	@Override
	public OperacaoDto converterEntityToDto(Operacao operacao) {
		return mapper.map(operacao, OperacaoDto.class);
	}

	@Override
	public List<Operacao> converterDtoToEntity(List<OperacaoDto> operacaoDto) {
		Type list = new TypeToken<List<Operacao>>() {}.getType();
		List<Operacao> lista = mapper.map(operacaoDto, list); 
		return lista;
	}

	@Override
	public List<OperacaoDto> converterEntityToDto(List<Operacao> operacao) {
		Type list = new TypeToken<List<Operacao>>() {}.getType();
		List<OperacaoDto> listDto = mapper.map(operacao, list);
		return listDto;
	}
	
	

}

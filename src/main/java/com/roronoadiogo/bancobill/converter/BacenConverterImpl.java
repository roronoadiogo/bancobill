package com.roronoadiogo.bancobill.converter;


import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roronoadiogo.bancobill.dtos.BacenDto;
import com.roronoadiogo.bancobill.entity.Bacen;

@Component
public class BacenConverterImpl implements IBacenConverter {
	
	@Autowired
	private ModelMapper mapper;
	
	@Override
	public BacenDto converterEntityToDto(Bacen bacen) {
		return mapper.map(bacen, BacenDto.class);
	}

	@Override
	public Bacen converterDtoToEntity(BacenDto bacenDto) {
		return mapper.map(bacenDto, Bacen.class);
	}

	@Override
	public List<BacenDto> converterEntityToDto(List<Bacen> bacens) {
		Type list = new TypeToken<List<Bacen>>() {}.getType();
		List<BacenDto> listaDto = mapper.map(bacens, list);
		return listaDto;
	}

	@Override
	public List<Bacen> converterDtoToEntity(List<BacenDto> bacensDto) {
		Type list = new TypeToken<List<BacenDto>>() {}.getType();
		List<Bacen> listaEntity = mapper.map(bacensDto, list);
		return listaEntity;
	}

}

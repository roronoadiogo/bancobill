package com.roronoadiogo.bancobill.converter;

import java.util.List;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.entity.Agencia;

public interface IAgenciaConverter {
	
	Agencia converterDtoToEntity(AgenciaDto agenciaDto);

	AgenciaDto converterEntityToDto(Agencia agencia);
	
	List<Agencia> converterDtoToEntity(List<AgenciaDto> agencia);
	
	List<AgenciaDto> converterEntityToDto(List<Agencia> agencia);

}

package com.roronoadiogo.bancobill.converter;

import java.util.List;

import com.roronoadiogo.bancobill.dtos.BancoDto;
import com.roronoadiogo.bancobill.entity.Banco;

public interface IBancoConverter {
	
	Banco converterDtoToEntity(BancoDto bancoDto);

	BancoDto converterEntityToDto(Banco banco);
	
	List<Banco> converterDtoToEntity(List<BancoDto> bancos);
	
	List<BancoDto> converterEntityToDto(List<Banco> bancos);
	
}

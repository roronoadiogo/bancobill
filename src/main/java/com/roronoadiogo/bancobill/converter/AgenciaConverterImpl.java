package com.roronoadiogo.bancobill.converter;


import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.entity.Agencia;


@Component
public class AgenciaConverterImpl implements IAgenciaConverter{

	@Autowired
	private ModelMapper mapper;
	
	@Override
	public Agencia converterDtoToEntity(AgenciaDto agenciaDto) {
		return mapper.map(agenciaDto, Agencia.class);
	}

	@Override
	public AgenciaDto converterEntityToDto(Agencia agencia) {
		return mapper.map(agencia, AgenciaDto.class);
	}

	@Override
	public List<Agencia> converterDtoToEntity(List<AgenciaDto> agencias) {
		
		Type list = new TypeToken<List<AgenciaDto>>() {}.getType();
		List<Agencia> agenciaList = mapper.map(agencias, list);
		return agenciaList;
	}

	@Override
	public List<AgenciaDto> converterEntityToDto(List<Agencia> agencia) {
		
		Type list = new TypeToken<List<Agencia>>() {}.getType();
		List<AgenciaDto> agenciaListDto = mapper.map(agencia, list);
		return agenciaListDto;
		
	}

}

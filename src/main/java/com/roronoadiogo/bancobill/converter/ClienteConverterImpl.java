package com.roronoadiogo.bancobill.converter;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.reflect.TypeToken;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.entity.Cliente;

@Component
public class ClienteConverterImpl implements IClienteConverter{
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public Cliente converterDtoToEntity(ClienteDto clienteDto) {
		return mapper.map(clienteDto, Cliente.class);
	}

	@Override
	public ClienteDto converterEntityToDto(Cliente cliente) {
		return mapper.map(cliente, ClienteDto.class);
	}

	@Override
	public List<Cliente> converterDtoToEntity(List<ClienteDto> clienteDtos) {
		
		Type list = new TypeToken<List<ClienteDto>>() {}.getType();
		List<Cliente> lista = mapper.map(clienteDtos, list); 
		return lista;
	}

	@Override
	public List<ClienteDto> converterEntityToDto(List<Cliente> clientes) {
		Type list = new TypeToken<List<Cliente>>() {}.getType();
		List<ClienteDto> listDto = mapper.map(clientes, list);
		return listDto;
	}
	
	

}

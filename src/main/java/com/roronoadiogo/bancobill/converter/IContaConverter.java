package com.roronoadiogo.bancobill.converter;

import java.util.List;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.entity.Cliente;
import com.roronoadiogo.bancobill.entity.Conta;

public interface IContaConverter {
	
	Conta converterDtoToEntity(ContaDto contaDto);

	ContaDto converterEntityToDto(Conta conta);
	
	List<Conta> converterDtoToEntity(List<ContaDto> contaDtos);
	
	List<ContaDto> converterEntityToDto(List<Conta> conta);

}

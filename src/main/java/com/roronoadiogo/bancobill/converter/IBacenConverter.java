package com.roronoadiogo.bancobill.converter;

import java.util.List;

import com.roronoadiogo.bancobill.dtos.BacenDto;
import com.roronoadiogo.bancobill.entity.Bacen;

public interface IBacenConverter {
	
	BacenDto converterEntityToDto(Bacen bacen);
	
	Bacen converterDtoToEntity(BacenDto bacenDto);
	
	List<BacenDto> converterEntityToDto(List<Bacen> bacensDto);

	List<Bacen> converterDtoToEntity(List<BacenDto> bacensDto);
}

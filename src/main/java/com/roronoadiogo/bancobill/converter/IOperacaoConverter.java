package com.roronoadiogo.bancobill.converter;

import java.util.List;

import com.roronoadiogo.bancobill.dtos.AgenciaDto;
import com.roronoadiogo.bancobill.dtos.ClienteDto;
import com.roronoadiogo.bancobill.dtos.ContaDto;
import com.roronoadiogo.bancobill.dtos.OperacaoDto;
import com.roronoadiogo.bancobill.entity.Agencia;
import com.roronoadiogo.bancobill.entity.Cliente;
import com.roronoadiogo.bancobill.entity.Conta;
import com.roronoadiogo.bancobill.entity.Operacao;

public interface IOperacaoConverter {
	
	Operacao converterDtoToEntity(OperacaoDto operacaoDto);

	OperacaoDto converterEntityToDto(Operacao operacao);
	
	List<Operacao> converterDtoToEntity(List<OperacaoDto> operacaoDto);
	
	List<OperacaoDto> converterEntityToDto(List<Operacao> operacao);

}

# Banco do Bill - BB

Projeto back-end desenvolvido na linguagem Java 
Pré-requisitos para aplicação:

  - Java 11
  - Spring Boot 2.1.6
  - Project Lombok [clique aqui](https://projectlombok.org/)
  - H2 Database
  - Mysql e JDBC para produção
  - Model Mapper [clique aqui](http://modelmapper.org/)


